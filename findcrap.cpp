#include <QtCore/Qfile>
#include <QtCore/QTextStream>
#include "findcrap.h"
#include "ui_findcrap.h"

findCrap::findCrap(QWidget *parent) : QWidget(parent), ui(new Ui::findCrap)
{
    ui->setupUi(this);
    getTextFile();
}

findCrap::~findCrap()
{
    delete ui;
}

void findCrap::on_goButton_clicked()
{
    QString matchTarget = ui->lineEdit->text();
    ui->textEdit->find(matchTarget, QTextDocument::FindWholeWords);
}

void findCrap::getTextFile()
{
    QFile myFile(":/anyTextFile.txt");
    myFile.open(QIODevice::ReadOnly);
    QTextStream textStream(&myFile);
    QString fileContents = textStream.readAll();
    myFile.close();

    ui->textEdit->setPlainText(fileContents);
    QTextCursor sailorsMouth = ui->textEdit->textCursor();
    sailorsMouth.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
}
